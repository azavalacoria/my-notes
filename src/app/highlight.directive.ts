import { Directive, ElementRef, Input } from '@angular/core';

@Directive({ selector: '[myHighlight], [toBeEdited]' })
export class HighlightDirective {
    constructor(el: ElementRef) {
       //el.nativeElement.style.color = 'blue';
       el.nativeElement.disabled = true;

    }
}

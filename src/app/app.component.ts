import { Component, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements AfterViewInit {

  show_form = false;
  form_label = '';
  formDisabled = true;

  new_note = {id: null, name: null, description: null};
  editing_note = {id: null, name: null, description: null};

  my_notes: any;

  constructor(public afDB: AngularFireDatabase) {
      this.my_notes = afDB.list('/notas').valueChanges().subscribe(
          notas => {
              this.my_notes=notas;
          });
  }

  @ViewChild('theInput') theInput: ElementRef;

  ngAfterViewInit() {
  }

  addNote(){
      this.form_label = "Guardar";
      this.formDisabled = false;
      this.clearNewNote();
      this.show_form = true;
  };

  createNote(){
      if(this.new_note.id == null) {
          this.new_note.id = Date.now();
          this.afDB.database.ref('notas/' + this.new_note.id).set(this.new_note);
          this.discardNote();
      } else if (this.formDisabled) {
          this.form_label = "Guardar Cambios";
          this.formDisabled = false;
      }
      else {
          this.afDB.database.ref('notas/' + this.new_note.id).set(this.new_note);
          this.discardNote();
      }
      //this.clearNewNote();
      //this.show_form = false;
      //this.formDisabled = true;
  }

  discardNote(){
      this.clearNewNote();
      this.show_form = false;
      this.formDisabled = true;
  };

  clearNewNote(){
      this.new_note = {id: null, name: null, description: null};
  };

  viewNote(note){
      this.form_label = "Editar";
      this.new_note = note;
      this.show_form = true;
      this.formDisabled = true;
  };

  getNotes(){
       return this.afDB.list('/notas').valueChanges();
  }

  deleteNote(){
      this.afDB.database.ref('notas/' + this.new_note.id).remove();
      this.show_form = false;
  }
}

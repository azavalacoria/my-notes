import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppComponent } from './app.component';
import { HighlightDirective } from './highlight.directive';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuthModule } from 'angularfire2/auth'

export const firebaseConfig = {
  apiKey: "AIzaSyDygirdpiTMBzBY80XRyO08xNB53y8TadU",
  authDomain: "ng-notas-azavalacoria.firebaseapp.com",
  databaseURL: "https://ng-notas-azavalacoria.firebaseio.com",
  storageBucket: "ng-notas-azavalacoria.appspot.com",
  messagingSenderId: '438527782516'
};

@NgModule({
  declarations: [
    AppComponent , HighlightDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
